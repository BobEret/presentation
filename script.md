#VSLIDE

# Digital Innovation at Forest

_Saul Samuels Moselle_

#VSLIDE

# BYOD

Forest has taken a huge step into the new technological world with its new ```Bring Your Own Device``` policy, and it's made huge strides. However, there are three main things that still hold it back. Firstly, and foremostly, the adoption of devices is not consistent, and this opens it up to abuse. After the initial phase of the BYOD initiative, many teachers settled into their own patterns. Some teachers barely use BYOD, and some use it quite significantly. This can render BYOD useless - some students feel that BYOD is useless, because they happen to have teachers that don't use the devices, while others feel that it is an excellent part of school. 

The second issue that School faces is the inconsistent abilities of students to use technology. The ASWFR, an essential part of School, has fallen to the wayside for many, as it is simply too difficult to access. For students who are not comfortable with using OneDrive consistently, or excel - which does not work well on iPads - it is almost impossible to use the ASWFR. The ASWFR is a special case, as it is the only form in school that is not done on the intranet, which makes it even more difficult. 

The third, and perhaps most obvious issue, is discipline with BYOD. It is very possible to break/loose concentration with a device, and students who do not care/are not motivated about their subjects can use them to almost switch off. Although some students are not technologically adept with software such as OneDrive, almost all are able to hide their lack of focus.  


# The Curriculum

The digital areas of teh curriculum at Forest are, I'm afraid to say, not always up to scratch. This area is an area wherein it is very difficult for any school to keep up - technology moves faster than schools, and much faster than textbooks or exam boards. But the new facilities and BYOD policy present a huge oppurtunity for Forest to become a leader in Computing and ICT, that are not fully utilisied. I believe that Forest can absolutely reach those goals, and I think that I have the digital skills, gained from CompSoc, Young Rewired State - a national app development competition that I have been a finalist of twice, to help school move forward in adopting the BYOD policy more fully, as well as education, especially in CompSoc - I have just prepared a course on Agile Development which will be taught from CompSoc, totry and push Forest into the digital age as I know it can be

