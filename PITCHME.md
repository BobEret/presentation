#VSLIDE

# Digital Innovation at Forest

_Saul Samuels Moselle_

#VSLIDE

# BYOD

### Inconsistency<!-- .element: class="fragment" -->
### Technical hurdles for students <!-- .element: class="fragment" -->
### Student discipline<!-- .element: class="fragment" -->

#VSLIDE

# Curriculum

### Technology is faster than exam boards<!-- .element: class="fragment" -->
